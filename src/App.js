import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  BoxGeometry,
  MeshBasicMaterial,
  Mesh,
  Matrix4
} from 'three';

export function init() {
  const scene = new Scene();
  const camera = new PerspectiveCamera(100, window.innerWidth / window.innerHeight, 0.1, 1000);

  const renderer = new WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  const geometry = new BoxGeometry();
  const material = new MeshBasicMaterial({ color: 0x00ff00 });
  const cube = new Mesh(geometry, material);
  scene.add(cube);

  camera.position.z = 10;

  function animate() {
    requestAnimationFrame(animate);

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
    const angle = Math.PI / 4
    const { sin, cos } = Math;
 
    cube.applyMatrix4(new Matrix4().set(...[
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      50, 50, 0, 1
    ]));

    renderer.render(scene, camera);
  }
  animate();
}
